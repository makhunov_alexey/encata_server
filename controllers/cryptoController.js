const CryptoCost = require('../models/cryptoCostModel');
const CryptoSymbol = require('../models/cryptoSymbolModel');
const axios = require('axios');


module.exports.getCryptoInformations = async (req, res, next) => {
    try {
        const allSymbolsInDB = await CryptoSymbol.find();
        const allInformation = await axios('https://api.hitbtc.com/api/2/public/ticker');
        allInformation.data.map((el)=>{
            let symbolInDB = allSymbolsInDB.find(x=> x.symbol == el.symbol)
            if(symbolInDB){
                let newCryptoCost = new CryptoCost({
                    value: el.open,
                    time: el.timestamp,
                    symbolID: symbolInDB._id
                });
                newCryptoCost.save();
                let newsymbolCostID = symbolInDB.symbolCostID;
                newsymbolCostID.push(newCryptoCost._id)
                symbolInDB.update({symbolCostID:newsymbolCostID}).exec();
            } else {
                let newCryptoSymbol = new CryptoSymbol({
                    symbol:el.symbol
                });
                let newCryptoCost = new CryptoCost({
                    value: el.open,
                    time: el.timestamp,
                    symbolID: newCryptoSymbol._id
                });
                newCryptoSymbol.symbolCostID.push(newCryptoCost._id);
                newCryptoCost.save();
                newCryptoSymbol.save();
            }
        });
        res.send();
    } catch (error) {
        res.status(422).send({errors:[{
            field:'error'
        }]});
    }
}

module.exports.getValueOfCrypto = async (req, res, next) => {
    try {
        let newInformation = [];
        let arrSymbols = [];
        let symbolsInformation = [];
        await CryptoSymbol.find(async function(err,result){
            if(result){
                result.map((el)=>{
                    arrSymbols.push(el.symbolCostID[el.symbolCostID.length-1]);
                    symbolsInformation.push({symbol:el.symbol,costID:el.symbolCostID[el.symbolCostID.length-1]})
                });
                let cryptoCosts = await CryptoCost.find({_id:arrSymbols});
                symbolsInformation.map((el)=>{
                    let information = cryptoCosts.find(x => x._id == el.costID.toString());
                    newInformation.push({symbol:el.symbol,value:information.value,time:information.time,personID:information.personID})
                });
                res.send(newInformation)
            }
        })
    } catch (error) {
        res.status(422).send({errors:[{
            field:'error'
        }]});
    }
}

module.exports.createNewCrypto = async (req, res, next) => {
    try {
        const symbol  = await CryptoSymbol.findOne({symbol:req.body.symbol});
        if(symbol){
            let newCryptoCost = new CryptoCost({
                value: req.body.value,
                time: req.body.time,
                symbolID: symbol._id,
                personID: req.body.personID
            });
            newCryptoCost.save();
            let newsymbolCostID = symbol.symbolCostID;
            newsymbolCostID.push(newCryptoCost._id)
            symbol.update({symbolCostID:newsymbolCostID}).exec();
            res.send();
        } else {
            let newCryptoSymbol = new CryptoSymbol({
                symbol:req.body.symbol
            });
            let newCryptoCost = new CryptoCost({
                value: req.body.value,
                time: req.body.time,
                symbolID: newCryptoSymbol._id,
                personID: req.body.personID
            });
            newCryptoSymbol.symbolCostID.push(newCryptoCost._id);
            newCryptoCost.save();
            newCryptoSymbol.save();
            res.send();
        }
    } catch (error) {
        res.status(422).send({errors:[{
            field:'error'
        }]});
    }
}