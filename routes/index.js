const express = require('express');
const router = express.Router();
const cryptoController = require('../controllers/cryptoController');


router.get('/getCryptoInformations',cryptoController.getCryptoInformations);
router.get('/getValueOfCrypto', cryptoController.getValueOfCrypto);
router.post('/createNewCrypto', cryptoController.createNewCrypto)

module.exports = router;
