const config = require('../config/config');
const Schema = config.mongoose.Schema;

const cryptoCostSchema = new Schema({
    value: {
        type: String,
        required: true
    },
    time: { 
        type: String,
        required: true
    },
    personID: {
        type: String,
        default:''
    },
    symbolID: {
        type: Schema.ObjectId,
        ref: 'CryptoSymbol',
        required: true
    }
}, 
    { versionKey: false }
);

const CryptoCost = config.mongoose.model("CryptoCost", cryptoCostSchema);
module.exports = CryptoCost;
