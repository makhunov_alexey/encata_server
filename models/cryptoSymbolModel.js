const config = require('../config/config');
const Schema = config.mongoose.Schema;

const cryptoSymbolSchema = new Schema({
    symbol: {
        type: String,
        required: true,
        unique: true
    },
    symbolCostID: [{
        type: Schema.ObjectId,
        ref: 'CryptoCost',
    }]
}, 
    { versionKey: false }
);

const CryptoSymbol = config.mongoose.model("CryptoSymbol", cryptoSymbolSchema);
module.exports = CryptoSymbol;
